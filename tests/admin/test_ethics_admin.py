import pytest
from model_bakery import baker

from django.contrib.admin.sites import AdminSite

from huscy.project_ethics.admin import EthicsAdmin
from huscy.project_ethics.models import Ethics

pytestmark = pytest.mark.django_db


@pytest.fixture
def admin():
    return EthicsAdmin(Ethics, AdminSite())


def test_project_title(admin):
    ethics = baker.make(Ethics, project__title='project_title')

    assert 'project_title' == admin.project_title(ethics)
