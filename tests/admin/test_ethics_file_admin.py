import pytest
from model_bakery import baker

from django.contrib.admin.sites import AdminSite

from huscy.project_ethics.admin import EthicsFileAdmin
from huscy.project_ethics.models import EthicsFile

pytestmark = pytest.mark.django_db


@pytest.fixture
def admin():
    return EthicsFileAdmin(EthicsFile, AdminSite())


@pytest.fixture
def ethics_file():
    return baker.make(
        'project_ethics.EthicsFile',
        ethics__code='123',
        ethics__ethics_committee__name='ethics_committee_name',
        ethics__project__title='project_title'
    )


def test_has_add_permission(admin, ethics_file):
    request = object()

    assert admin.has_add_permission(request) is False
    assert admin.has_add_permission(request, ethics_file) is False


def test_ethics_committee(admin, ethics_file):
    assert 'ethics_committee_name' == admin.ethics_committee(ethics_file)


def test_ethics_code(admin, ethics_file):
    assert '123' == admin.ethics_code(ethics_file)


def test_project_title(admin, ethics_file):
    assert 'project_title' == admin.project_title(ethics_file)
