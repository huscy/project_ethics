import pytest
from pytest_bdd import given, parsers, then
from model_bakery import baker

from django.contrib.auth.models import Permission
from rest_framework.test import APIClient

from huscy.projects.services import create_membership


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='Erik', last_name='Zion')


@pytest.fixture
def staff_user(user):
    user.is_staff = True
    user.save()
    return user


@given('I am admin user', target_fixture='client')
def admin_client(api_client, admin_user):
    api_client.login(username=admin_user.username, password='password')
    return api_client


@given('I am staff user', target_fixture='client')
def staff_user_client(api_client, staff_user):
    api_client.login(username=staff_user.username, password='password')
    return api_client


@given('I am normal user', target_fixture='client')
def user_client(api_client, user):
    api_client.login(username=user.username, password='password')
    return api_client


@given('I am anonymous user', target_fixture='client')
def anonymous_client(api_client):
    return api_client


@pytest.fixture
def project():
    return baker.make('projects.Project')


@pytest.fixture
def ethics_committee():
    return baker.make('project_ethics.EthicsCommittee')


@pytest.fixture
def ethics(ethics_committee, project):
    return baker.make('project_ethics.Ethics', ethics_committee=ethics_committee, project=project)


@pytest.fixture
def ethics_file(ethics):
    return baker.make('project_ethics.EthicsFile', ethics=ethics)


@pytest.fixture
def tmp_file(tmp_path):
    _file = tmp_path / 'tmp.pdf'
    _file.write_text('foo bar')
    return _file


@given(parsers.parse('I have {codename} permission'), target_fixture='codename')
def assign_permission(user, codename):
    permission = Permission.objects.get(codename=codename)
    user.user_permissions.add(permission)


@given('I am project coordinator')
def coordinator_membership(user, project):
    create_membership(project, user, is_coordinator=True)


@given('I am project member with write permission')
def membership_with_write_permission(user, project):
    create_membership(project, user, has_write_permission=True)


@given('I am project member with read permission')
def membership_with_read_permission(user, project):
    create_membership(project, user)


@then(parsers.parse('I get status code {status_code:d}'))
def assert_status_code(request_result, status_code):
    assert request_result.status_code == status_code
