import pytest

from huscy.project_ethics.services import create_ethics

pytestmark = pytest.mark.django_db


def test_create_ethics(project, ethics_committee):
    result = create_ethics(project, ethics_committee)

    assert result.project == project
    assert result.ethics_committee is ethics_committee
    assert result.code == ''


def test_create_with_code(project, ethics_committee):
    result = create_ethics(project, ethics_committee, '1234567890')

    assert result.project == project
    assert result.ethics_committee is ethics_committee
    assert result.code == '1234567890'
