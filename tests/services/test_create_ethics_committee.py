import pytest

from huscy.project_ethics.models import EthicsCommittee
from huscy.project_ethics.services import create_ethics_committee

pytestmark = pytest.mark.django_db


def test_create_ethics_committee():
    result = create_ethics_committee('ethics committee name')

    assert isinstance(result, EthicsCommittee)
    assert result.name == 'ethics committee name'
