from datetime import datetime

from freezegun import freeze_time

from django.core.files.uploadedfile import SimpleUploadedFile

from huscy.project_ethics.models import EthicsFile
from huscy.project_ethics.services import create_ethics_file


@freeze_time('2020-01-01 10:00:00')
def test_create_ethics_file(user, ethics, tmp_file):
    filetype = EthicsFile.TYPE.proposal

    with open(tmp_file, 'rb') as fh:
        filehandle = SimpleUploadedFile('my_filename.txt', fh.read())
        result = create_ethics_file(ethics, filehandle, filetype, user)

    assert result.ethics == ethics
    assert result.filetype == filetype
    assert result.filename == 'my_filename.txt'
    assert result.uploaded_at == datetime(2020, 1, 1, 10)
    assert result.uploaded_by == 'Erik Zion'
