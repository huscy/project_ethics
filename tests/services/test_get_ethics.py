from itertools import cycle

import pytest
from model_bakery import baker

from huscy.project_ethics.services import get_ethics

pytestmark = pytest.mark.django_db


@pytest.fixture
def projects():
    return baker.make('projects.Project', _quantity=3)


@pytest.fixture
def ethics_committees():
    ethics_committee_names = [f'Ethics Committee {letter}' for letter in 'ABCDEF']
    # the names are important because of ordering
    return baker.make('project_ethics.EthicsCommittee', name=cycle(ethics_committee_names),
                      _quantity=6)


@pytest.fixture
def ethics(projects, ethics_committees):
    return baker.make('project_ethics.Ethics', project=cycle(projects),
                      ethics_committee=cycle(ethics_committees), _quantity=6)


def test_get_ethics_filtered_by_project(projects, ethics):
    result = get_ethics(projects[0])

    assert len(result) == 2
    assert list(result) == [ethics[0], ethics[3]]
