from itertools import cycle

import pytest
from model_bakery import baker

from huscy.project_ethics.services import get_ethics_committees

pytestmark = pytest.mark.django_db


def test_ordering():
    baker.make('project_ethics.EthicsCommittee', name=cycle('A C D B'.split()), _quantity=4)

    result = get_ethics_committees()

    assert ['A', 'B', 'C', 'D'] == [ethics_committee.name for ethics_committee in result]
