from itertools import cycle

import pytest
from model_bakery import baker

from huscy.project_ethics.services import get_ethics_files

pytestmark = pytest.mark.django_db


@pytest.fixture
def ethics_files(ethics):
    baker.make('project_ethics.EthicsFile', filename=cycle('EFGH'), _quantity=4)
    return baker.make('project_ethics.EthicsFile', ethics=ethics, filename=cycle('BADC'),
                      _quantity=4)


def test_ordering(ethics, ethics_files):
    result = get_ethics_files(ethics)

    assert list(result) == [
        ethics_files[1],  # filename: A
        ethics_files[0],  # filename: B
        ethics_files[3],  # filename: C
        ethics_files[2],  # filename: D
    ]
