import pytest
from model_bakery import baker

from huscy.project_ethics.services import update_ethics

pytestmark = pytest.mark.django_db


def test_update_ethics(ethics):
    ethics_committee = baker.make('project_ethics.EthicsCommittee')

    result = update_ethics(ethics, ethics_committee, 'new ethics code')

    ethics.refresh_from_db()
    assert result == ethics
    assert result.ethics_committee == ethics_committee
    assert result.code == 'new ethics code'
