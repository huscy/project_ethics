import pytest

from huscy.project_ethics.services import update_ethics_committee

pytestmark = pytest.mark.django_db


def test_update_ethics_committee(ethics_committee):
    result = update_ethics_committee(ethics_committee, 'new ethics committee name')

    ethics_committee.refresh_from_db()
    assert result == ethics_committee
    assert result.name == 'new ethics committee name'
