import pytest

from huscy.project_ethics.models import EthicsFile
from huscy.project_ethics.services import update_ethics_file

pytestmark = pytest.mark.django_db


def test_update_ethics_file(ethics_file):
    filetype = EthicsFile.TYPE.amendment
    result = update_ethics_file(ethics_file, filetype, 'the_filename')

    assert result.filetype == filetype
    assert result.filename == 'the_filename'

    ethics_file.refresh_from_db()
    assert result == ethics_file
