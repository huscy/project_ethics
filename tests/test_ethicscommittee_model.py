from huscy.project_ethics.models import EthicsCommittee


def test_str():
    ethics_committee = EthicsCommittee(name='ethics committee name')

    assert 'ethics committee name' == str(ethics_committee)
