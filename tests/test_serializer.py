import pytest
from model_bakery import baker

from huscy.project_ethics.serializer import EthicsSerializer


pytestmark = pytest.mark.django_db


def test_ethics_serializer(ethics):
    baker.make('project_ethics.EthicsFile', ethics=ethics, _quantity=3)

    data = EthicsSerializer(ethics).data

    assert data['code'] == ethics.code
    assert data['ethics_committee'] == ethics.ethics_committee.pk
    assert data['ethics_committee_name'] == ethics.ethics_committee.name
    assert len(data['ethics_files']) == 3
    assert data['id'] == ethics.pk
    assert data['project'] == ethics.project.pk
