import pytest
from pytest_bdd import scenarios, when

from rest_framework.reverse import reverse

from huscy.project_ethics.models import EthicsFile

pytestmark = pytest.mark.django_db

scenarios(
    'viewsets/create_ethics.feature',
    'viewsets/create_ethicscommittee.feature',
    'viewsets/create_ethicsfile.feature',
    'viewsets/delete_ethics.feature',
    'viewsets/delete_ethicscommittee.feature',
    'viewsets/delete_ethicsfile.feature',
    'viewsets/update_ethics.feature',
    'viewsets/update_ethicscommittee.feature',
    'viewsets/update_ethicsfile.feature',
    'viewsets/view_ethics.feature',
    'viewsets/view_ethicscommittee.feature',
    'viewsets/view_ethicsfile.feature',
)


@when('I try to create ethics', target_fixture='request_result')
def create_ethics(client, project, ethics_committee):
    data = dict(
        code='123/12-ek',
        ethics_committee=ethics_committee.id,
    )
    return client.post(reverse('ethics-list', kwargs=dict(project_pk=project.pk)), data=data)


@when('I try to create an ethics committee', target_fixture='request_result')
def create_ethics_committee(client):
    return client.post(reverse('ethicscommittee-list'), data=dict(name='foo Bar'))


@when('I try to create an ethics file', target_fixture='request_result')
def create_ethics_file(client, ethics, tmp_file):
    with open(tmp_file, 'r') as f:
        data = dict(
            filehandle=f,
            filetype=EthicsFile.TYPE.proposal,
        )
        return client.post(
            reverse(
                'ethicsfile-list',
                kwargs=dict(project_pk=ethics.project.pk, ethics_pk=ethics.pk)
            ),
            data=data
        )


@when('I try to delete ethics', target_fixture='request_result')
def delete_ethics(client, ethics):
    return client.delete(
        reverse('ethics-detail', kwargs=dict(project_pk=ethics.project.pk, pk=ethics.pk))
    )


@when('I try to delete an ethics committee', target_fixture='request_result')
def delete_ethics_committee(client, ethics_committee):
    return client.delete(reverse('ethicscommittee-detail', kwargs=dict(pk=ethics_committee.pk)))


@when('I try to delete an ethics file', target_fixture='request_result')
def delete_ethics_file(client, ethics, ethics_file):
    return client.delete(
        reverse(
            'ethicsfile-detail',
            kwargs=dict(project_pk=ethics.project.pk, ethics_pk=ethics.pk, pk=ethics_file.pk)
        )
    )


@when('I try to list ethics', target_fixture='request_result')
def list_ethics(client, project):
    return client.get(reverse('ethics-list', kwargs=dict(project_pk=project.pk)))


@when('I try to list ethics committees', target_fixture='request_result')
def list_ethics_committees(client):
    return client.get(reverse('ethicscommittee-list'))


@when('I try to list ethics files', target_fixture='request_result')
def list_ethics_files(client, project, ethics):
    return client.get(
        reverse('ethicsfile-list', kwargs=dict(project_pk=project.pk, ethics_pk=ethics.pk))
    )


@when('I try to patch ethics', target_fixture='request_result')
def patch_ethics(client, ethics):
    return client.patch(
        reverse('ethics-detail', kwargs=dict(project_pk=ethics.project.pk, pk=ethics.pk))
    )


@when('I try to patch an ethics committee', target_fixture='request_result')
def patch_ethics_committee(client, ethics_committee):
    return client.patch(reverse('ethicscommittee-detail', kwargs=dict(pk=ethics_committee.pk)))


@when('I try to patch an ethics file', target_fixture='request_result')
def patch_ethics_file(client, ethics_file):
    return client.patch(
        reverse(
            'ethicsfile-detail',
            kwargs=dict(project_pk=ethics_file.ethics.project.pk,
                        ethics_pk=ethics_file.ethics.pk,
                        pk=ethics_file.pk)
        )
    )


@when('I try to retrieve ethics', target_fixture='request_result')
def retrieve_ethics(client, ethics):
    return client.get(
        reverse('ethics-detail', kwargs=dict(project_pk=ethics.project.pk, pk=ethics.pk))
    )


@when('I try to retrieve an ethics committee', target_fixture='request_result')
def retrieve_ethics_committee(client, ethics_committee):
    return client.get(reverse('ethicscommittee-detail', kwargs=dict(pk=ethics_committee.pk)))


@when('I try to retrieve an ethics file', target_fixture='request_result')
def retrieve_ethics_file(client, ethics_file):
    return client.get(
        reverse(
            'ethicsfile-detail',
            kwargs=dict(project_pk=ethics_file.ethics.project.pk,
                        ethics_pk=ethics_file.ethics.pk,
                        pk=ethics_file.pk)
        )
    )


@when('I try to update ethics', target_fixture='request_result')
def update_ethics(client, ethics):
    return client.put(
        reverse('ethics-detail', kwargs=dict(project_pk=ethics.project.pk, pk=ethics.pk)),
        data=dict(ethics_committee=ethics.ethics_committee.pk, code='foobar')
    )


@when('I try to update an ethics committee', target_fixture='request_result')
def update_ethics_committee(client, ethics_committee):
    return client.put(
        reverse('ethicscommittee-detail', kwargs=dict(pk=ethics_committee.pk)),
        data=dict(name='new ethics committee name')
    )


@when('I try to update an ethics file', target_fixture='request_result')
def update_ethics_file(client, ethics_file):
    return client.put(
        reverse('ethicsfile-detail', kwargs=dict(project_pk=ethics_file.ethics.project.pk,
                                                 ethics_pk=ethics_file.ethics.pk,
                                                 pk=ethics_file.pk)),
        data=dict(filetype=2, filename='new_file_name')
    )
