Feature: create ethics committee

	Scenario: create ethics committee as admin user
		Given I am admin user
		When I try to create an ethics committee
		Then I get status code 201

	Scenario: create ethics committee as staff user
		Given I am staff user
		When I try to create an ethics committee
		Then I get status code 403

	Scenario: create ethics committee with add_ethicscommittee permission
		Given I am normal user
		And I have add_ethicscommittee permission
		When I try to create an ethics committee
		Then I get status code 201

	Scenario: create ethics committee as normal user
		Given I am normal user
		When I try to create an ethics committee
		Then I get status code 403

	Scenario: create ethics committee as anonymous user
		Given I am anonymous user
		When I try to create an ethics committee
		Then I get status code 403
