Feature: delete ethics committee

	Scenario: delete ethics committee as admin user
		Given I am admin user
		When I try to delete an ethics committee
		Then I get status code 204

	Scenario: delete ethics committee as staff user
		Given I am staff user
		When I try to delete an ethics committee
		Then I get status code 403

	Scenario: delete ethics committee with delete_ethicscommittee permission
		Given I am normal user
		And I have delete_ethicscommittee permission
		When I try to delete an ethics committee
		Then I get status code 204

	Scenario: delete ethics committee as normal user
		Given I am normal user
		When I try to delete an ethics committee
		Then I get status code 403

	Scenario: delete ethics committee as anonymous user
		Given I am anonymous user
		When I try to delete an ethics committee
		Then I get status code 403
