Feature: delete ethics file

	Scenario: delete ethics file as admin user
		Given I am admin user
		When I try to delete an ethics file
		Then I get status code 204

	Scenario: delete ethics file as staff user
		Given I am staff user
		When I try to delete an ethics file
		Then I get status code 403

	Scenario: delete ethics file with change_project permissions
		Given I am normal user
		And I have change_project permission
		When I try to delete an ethics file
		Then I get status code 403

	Scenario: delete ethics file with delete_ethicsfile permissions
		Given I am normal user
		And I have delete_ethicsfile permission
		When I try to delete an ethics file
		Then I get status code 403

	Scenario: delete ethics file with change_project and delete_ethicsfile permissions
		Given I am normal user
		And I have change_project permission
		And I have delete_ethicsfile permission
		When I try to delete an ethics file
		Then I get status code 204

	Scenario: delete ethics file as project coordinator
		Given I am normal user
		And I am project coordinator
		When I try to delete an ethics file
		Then I get status code 204

	Scenario: delete ethics file as project member with write permission
		Given I am normal user
		And I am project member with write permission
		When I try to delete an ethics file
		Then I get status code 403

	Scenario: delete ethics file as project member with read permission
		Given I am normal user
		And I am project member with read permission
		When I try to delete an ethics file
		Then I get status code 403

	Scenario: delete ethics file as normal user
		Given I am normal user
		When I try to delete an ethics file
		Then I get status code 403

	Scenario: delete ethics file as anonymous user
		Given I am anonymous user
		When I try to delete an ethics file
		Then I get status code 403
