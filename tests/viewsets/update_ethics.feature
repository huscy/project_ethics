Feature: update ethics

	Scenario: patch ethics is not allowed
		Given I am admin user

		When I try to patch ethics

		Then I get status code 405

	Scenario: update ethics as admin user
		Given I am admin user

		When I try to update ethics

		Then I get status code 200

	Scenario: update ethics as staff user
		Given I am staff user

		When I try to update ethics

		Then I get status code 403

	Scenario: update ethics with change_project permission
		Given I am normal user
		And I have change_project permission

		When I try to update ethics

		Then I get status code 200

	Scenario: update ethics as project coordinator
		Given I am normal user
		And I am project coordinator

		When I try to update ethics

		Then I get status code 200

	Scenario: update ethics as project member with write permission
		Given I am normal user
		And I am project member with write permission

		When I try to update ethics

		Then I get status code 200

	Scenario: update ethics as project member with read permission
		Given I am normal user
		And I am project member with read permission

		When I try to update ethics

		Then I get status code 403

	Scenario: update ethics as normal user
		Given I am normal user

		When I try to update ethics

		Then I get status code 403

	Scenario: update ethics as anonymous user
		Given I am anonymous user

		When I try to update ethics

		Then I get status code 403
