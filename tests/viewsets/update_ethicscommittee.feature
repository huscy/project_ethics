Feature: update ethics committee

	Scenario: patch ethics committee is not allowed
		Given I am admin user
		When I try to patch an ethics committee
		Then I get status code 405

	Scenario: update ethics committee as admin user
		Given I am admin user
		When I try to update an ethics committee
		Then I get status code 200

	Scenario: update ethics committee as staff user
		Given I am staff user
		When I try to update an ethics committee
		Then I get status code 403

	Scenario: update ethics committee with change_ethicscommittee permission
		Given I am normal user
		And I have change_ethicscommittee permission
		When I try to update an ethics committee
		Then I get status code 200

	Scenario: update ethics committee as normal user
		Given I am normal user
		When I try to update an ethics committee
		Then I get status code 403

	Scenario: update ethics committee as anonymous user
		Given I am anonymous user
		When I try to update an ethics committee
		Then I get status code 403
