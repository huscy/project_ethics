Feature: view ethics

	Scenario: retrieve ethics is not allowed
		Given I am admin user

		When I try to retrieve ethics

		Then I get status code 405

	Scenario: list ethics as admin user
		Given I am admin user

		When I try to list ethics

		Then I get status code 200

	Scenario: list ethics as staff user
		Given I am staff user

		When I try to list ethics

		Then I get status code 200

	Scenario: list ethics as normal user
		Given I am normal user

		When I try to list ethics

		Then I get status code 200

	Scenario: list ethics as anonymous user
		Given I am anonymous user

		When I try to list ethics

		Then I get status code 403
