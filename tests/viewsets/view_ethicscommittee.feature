Feature: view ethics committee

	Scenario: retrieve ethics committee is not allowed
		Given I am admin user

		When I try to retrieve an ethics committee

		Then I get status code 405

	Scenario: list ethics committees as admin user
		Given I am admin user

		When I try to list ethics committees

		Then I get status code 200

	Scenario: list ethics committees as staff user
		Given I am staff user

		When I try to list ethics committees

		Then I get status code 200

	Scenario: list ethics committees as normal user
		Given I am normal user

		When I try to list ethics committees

		Then I get status code 200

	Scenario: list ethics committees as anonymous user
		Given I am anonymous user

		When I try to list ethics committees

		Then I get status code 403
