Feature: view ethics files

	Scenario: list ethics files is not allowed
		Given I am normal user

		When I try to list ethics files

		Then I get status code 405

	Scenario: retrieve ethics files is not allowed
		Given I am normal user

		When I try to retrieve an ethics file

		Then I get status code 405
